package models;

import java.math.BigDecimal;
import java.util.List;

public class Output {

    private int countryVisits;

    private List<Country> countryList;

    private BigDecimal leftover;

    private String currency;

    public Output() {
    }

    public int getCountryVisits() {
        return countryVisits;
    }

    public void setCountryVisits(int countryVisits) {
        this.countryVisits = countryVisits;
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public BigDecimal getLeftover() {
        return leftover;
    }

    public void setLeftover(BigDecimal leftover) {
        this.leftover = leftover;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
