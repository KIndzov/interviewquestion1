package models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.List;

public class Country {

    private String name;

    private String code;

    private String alternativeCode;

    private String currency;

    private BigDecimal money;

    private List<Country> neighbours;

    private List<String> neighbourCodes;

    public Country() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAlternativeCode() {
        return alternativeCode;
    }

    public void setAlternativeCode(String alternativeCode) {
        this.alternativeCode = alternativeCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @JsonIgnore
    public List<Country> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<Country> neighbours) {
        this.neighbours = neighbours;
    }

    @JsonIgnore
    public List<String> getNeighbourCodes() {
        return neighbourCodes;
    }

    public void setNeighbourCodes(List<String> neighbourCodes) {
        this.neighbourCodes = neighbourCodes;
    }
}
