package models;

import java.math.BigDecimal;

public class Input {

    private String countryCode;

    private BigDecimal startMoney;

    private BigDecimal moneyPerCountry;

    private String currency;

    public Input() {
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public BigDecimal getStartMoney() {
        return startMoney;
    }

    public void setStartMoney(BigDecimal startMoney) {
        this.startMoney = startMoney;
    }

    public BigDecimal getMoneyPerCountry() {
        return moneyPerCountry;
    }

    public void setMoneyPerCountry(BigDecimal moneyPerCountry) {
        this.moneyPerCountry = moneyPerCountry;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
