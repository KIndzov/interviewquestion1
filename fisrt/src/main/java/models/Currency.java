package models;

import java.math.BigDecimal;

public class Currency {

    private String currencyName;

    private String id;

    private BigDecimal toUSD;

    private BigDecimal fromUSD;

    public Currency() {
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getToUSD() {
        return toUSD;
    }

    public void setToUSD(BigDecimal toUSD) {
        this.toUSD = toUSD;
    }

    public BigDecimal getFromUSD() {
        return fromUSD;
    }

    public void setFromUSD(BigDecimal fromUSD) {
        this.fromUSD = fromUSD;
    }
}
