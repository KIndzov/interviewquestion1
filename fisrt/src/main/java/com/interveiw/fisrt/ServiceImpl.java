package com.interveiw.fisrt;


import com.interveiw.fisrt.contracts.Connector;
import com.interveiw.fisrt.contracts.Service;
import models.Country;
import models.Output;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@org.springframework.stereotype.Service
public class ServiceImpl implements Service {

    private final Connector connector;

    @Autowired
    public ServiceImpl(Connector connector) {
        this.connector = connector;
    }


    @Override
    public Output function(String countryCode, BigDecimal startMoney, BigDecimal moneyPerCountry, String currency) {
        if(startMoney.compareTo(BigDecimal.ZERO) <= 0 || moneyPerCountry.compareTo(BigDecimal.ZERO) <=0){
            throw new IllegalArgumentException("Cannot have less than Zero money");
        }
        Output output = new Output();
        Map<String,Country> countryMap = getCountries();
        int neighbours = getCountryNeighboursSize(countryCode,countryMap);
        int visits = 0;
        output.setCountryList(getNeighboursList(countryCode,countryMap));

        if(neighbours != 0){
            visits = startMoney.divide(moneyPerCountry, RoundingMode.DOWN).divide(new BigDecimal(neighbours),RoundingMode.DOWN).intValue();
            output.setLeftover(startMoney.subtract(moneyPerCountry.multiply(new BigDecimal(visits)).multiply(new BigDecimal(neighbours))));
            setCountryBudgets(output.getCountryList(),visits,moneyPerCountry,currency);
        } else {
            output.setLeftover(startMoney);
        }

        output.setCountryVisits(visits);


        output.setCurrency(currency);

        return output;



    }

    @Override
    public void updateDatabase() {
        Map<String,Country> countryList = new HashMap<>();
        countryList = connector.getAllCountries();
        writeToFile(countryList);
    }

    private void writeToFile(Map<String, Country> countryList) {
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("countries.txt"));
            List<Country> countryList1 = new ArrayList<>(countryList.values());
            for (Country country : countryList1) {
                writer.append(country.getName()).append(",");
                writer.append(country.getCode()).append(",");
                writer.append(country.getAlternativeCode()).append(",");
                writer.append(country.getCurrency()).append(",");
                for (Country neighbour : country.getNeighbours()) {
                    writer.append(neighbour.getCode()).append(";");
                }
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<String,Country> getCountries(){
        Map<String,Country> countryMap = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("countries.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                Country country = new Country();
                String[] attributes = line.split(",");
                country.setName(attributes[0]);
                country.setCode(attributes[1]);
                country.setAlternativeCode(attributes[2]);
                country.setCurrency(attributes[3]);
                if(attributes.length == 5) {
                    String[] neighbours = attributes[4].split(";");
                    List<String> neighbourCodes = new ArrayList<>(Arrays.asList(neighbours));
                    country.setNeighbourCodes(neighbourCodes);
                } else {
                    country.setNeighbourCodes(new ArrayList<>());
                }
                countryMap.put(attributes[1],country);
            }
            reader.close();
            for (Country country : countryMap.values()) {
                List<Country> neighbours = new ArrayList<>();
                for (String neighbourCode : country.getNeighbourCodes()) {
                    Country neighbour = getCountryByCode(neighbourCode,countryMap);
                    neighbours.add(neighbour);
                }
                country.setNeighbours(neighbours);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

             return countryMap;
    }

    private List<Country> getNeighboursList(String countryCode, Map<String,Country> countryMap) {

        return getCountryByCode(countryCode,countryMap).getNeighbours();
//        return connector.getCountryNeighbours(countryCode);
    }

    private int getCountryNeighboursSize(String countryCode,Map<String,Country> countryMap) {
        return getNeighboursList(countryCode,countryMap).size();
    }

    private void setCountryBudgets(List<Country> countryList, int visits, BigDecimal moneyPerCountry, String currency){
        for (Country country : countryList) {
            BigDecimal countryMoney = moneyPerCountry;
            countryMoney = countryMoney.multiply(new BigDecimal(connector.getRateTo(currency)));
            countryMoney = (new BigDecimal(visits)).multiply(countryMoney).multiply(new BigDecimal(connector.getRateFrom(country.getCurrency())));
            country.setMoney(countryMoney.setScale(2,RoundingMode.DOWN));
        }

    }

    private Country getCountryByCode(String code, Map<String,Country> countryMap){
            return countryMap.get(code);
    }



}
