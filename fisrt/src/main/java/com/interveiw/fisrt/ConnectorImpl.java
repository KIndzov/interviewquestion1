package com.interveiw.fisrt;

import com.interveiw.fisrt.contracts.Connector;
import models.Country;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ConnectorImpl implements Connector {

    @Override
    public List<Country> getCountryNeighbours(String countryCode) {
        final String uri = "https://restcountries.eu/rest/v2/alpha/" + countryCode;
        RestTemplate restTemplate = new RestTemplate();
        List<Country> neighbourCountries = new ArrayList<>();

        try {
            var result = restTemplate.getForObject(uri, Map.class);
            List<String> neighbourCodes = (List<String>) result.get("borders");


        for (String neighbourCode : neighbourCodes) {
            Country country = getCountry(neighbourCode);
            neighbourCountries.add(country);
        }
        }catch (HttpClientErrorException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unsupported CountryCode");
        }

        return neighbourCountries;


    }

    @Override
    public Country getCountry(String countryCode) {

        final String uri = "https://restcountries.eu/rest/v2/alpha/" + countryCode;
        RestTemplate restTemplate = new RestTemplate();

        var result = restTemplate.getForObject(uri, Map.class);

        Country country = new Country();

        if(result == null || result.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        country.setCode(countryCode);
        List<Map<String,String>> currency = (List<Map<String, String>>) result.get("currencies");
        country.setCurrency(currency.get(0).get("code"));
        country.setName((String) result.get("name"));
        country.setMoney(new BigDecimal(0));

        return country;
    }

    @Override
    public double getRateTo(String currency) {
        String exchange = currency +"_USD";
        final String uri = "https://free.currconv.com/api/v7/convert?q=" + exchange +"&compact=ultra&apiKey=8ce206f7ec201e33b03e";
        Double money;
        try {
            RestTemplate restTemplate = new RestTemplate();
            var result = restTemplate.getForObject(uri, Map.class);

            money = Double.parseDouble(result.get(exchange).toString());

        }catch (NullPointerException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unsupported CurrencyCode");
        }

        return money;

    }

    @Override
    public double getRateFrom(String currency) {
        String exchange = "USD_" + currency;
        final String uri = "https://free.currconv.com/api/v7/convert?q=" + exchange +"&compact=ultra&apiKey=8ce206f7ec201e33b03e";
        RestTemplate restTemplate = new RestTemplate();

        var result = restTemplate.getForObject(uri, Map.class);

        return Double.parseDouble(result.get(exchange).toString());
    }

    @Override
    public Map<String, Country> getAllCountries() {
        final String uri = "https://restcountries.eu/rest/v2/all";
        RestTemplate restTemplate = new RestTemplate();

        Map<String,Country> countryMap = new HashMap<>();

        var result = restTemplate.getForObject(uri,List.class);

        for (Object country : result) {
            Map<String,Object> workCountry = (Map<String, Object>) country;
            Country newCountry = new Country();
            newCountry.setName((String) workCountry.get("name"));
            String countryCode = (String) workCountry.get("alpha3Code");
            newCountry.setCode(countryCode);
            newCountry.setAlternativeCode((String) workCountry.get("alpha2Code"));
            List<Map<String,Object>> currencyLIst = (List<Map<String,Object>>) workCountry.get("currencies");
            newCountry.setCurrency((String) currencyLIst.get(0).get("code"));
            countryMap.put(countryCode,newCountry);
        }

        for (Object country : result) {
            Map<String,Object> workCountry = (Map<String, Object>) country;
            String countryCode = (String) workCountry.get("alpha3Code");
            Country countryToAddNeighbours = countryMap.get(countryCode);
            List<String> neighbourCodes = (List<String>) workCountry.get("borders");
            List<Country> neighbourCountries = new ArrayList<>();
            for (String neighbourCode : neighbourCodes) {
                Country countryToAdd = countryMap.get(neighbourCode);
                neighbourCountries.add(countryToAdd);
            }
            try {
                countryToAddNeighbours.setNeighbours(neighbourCountries);
            } catch (RuntimeException e){
                System.out.println("Error");
            }


        }
        return countryMap;
    }
}
