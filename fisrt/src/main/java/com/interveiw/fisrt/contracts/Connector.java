package com.interveiw.fisrt.contracts;

import models.Country;

import java.util.List;
import java.util.Map;

public interface Connector {
    List<Country> getCountryNeighbours(String countryCode);

    Country getCountry(String countryCode);

    double getRateTo(String currency);

    double getRateFrom(String currency);

    Map<String,Country> getAllCountries();
}
