package com.interveiw.fisrt.contracts;

import models.Output;

import java.math.BigDecimal;

public interface Service {


    Output function(String countryCode, BigDecimal startMoney, BigDecimal moneyPerCountry, String currency);

    void updateDatabase();
}
