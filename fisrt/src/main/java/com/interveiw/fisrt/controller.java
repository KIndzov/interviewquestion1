package com.interveiw.fisrt;

import com.interveiw.fisrt.contracts.Service;
import models.Output;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Locale;

@RestController
@RequestMapping("/v1")
public class controller {

    private final Service service;


    @Autowired
    public controller(Service service) {
        this.service = service;
    }

    @GetMapping("/work")
    public Output function(@RequestParam String countryCode,
                           @RequestParam BigDecimal startMoney,
                           @RequestParam BigDecimal moneyPerCountry,
                           @RequestParam String currency) {
        try {
            return service.function(countryCode.toUpperCase(Locale.ROOT), startMoney, moneyPerCountry, currency.toUpperCase());
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }


    }

    @GetMapping("/update")
    public void fillDatabase() {
        try {
            service.updateDatabase();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }



}
